*** Settings ***
Library  AppiumLibrary

*** Test Cases ***
Test open application&login
    [Documentation]   application&login  
        ...            Login
        ...            1. เข้า App iServiceMax
        ...            2. ใส่ Username และ Password
        ...            3. กดปุ่ม Login
        ...            4. บันทึกภาพหน้าจอ
        ...            5. กดปุ่ม Logout
    Open-Application
    Login
    Logout
*** Keywords ***
Open-Application
    Open Application    http://localhost:4723/wd/hub    platformName=Android    platformVersion=12  deviceName=emulator-5554    appPackage=th.co.truecorp.cpd.iservicemax  appActivity=th.co.truecorp.cpd.iservicemax.MainActivity  autoGrantPermissions=true
Login
    Wait Until Page Contains Element  //android.widget.Button[@content-desc="button_login"]  15
    Input Text  //*[@text='Username']  39027299
    Input Text  //*[@text='Password']  Test=12345
    Click Element  //*[@text='Login']
    Wait Until Page Contains Element  //*[@text='Smart Visit']  15
    Capture Page Screenshot  none
Logout
    Click Element   //android.widget.TextView[@index='0']
    Wait Until Page Contains Element  //android.widget.ScrollView 
    Click Element   //*[@text='Logout']
    Close Application