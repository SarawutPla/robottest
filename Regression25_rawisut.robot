*** Settings ***
Library    AppiumLibrary


*** Test Cases ***  
Test open application
    [Documentation]
        ...    1. เปิด app iServiceMax
        ...    2. ใส่ Username & Password
        ...    3. กดปุ่ม Login
        ...    4. บันทึกภาพหน้าจอ
        ...    5. กดปุ่ม Logout
        ...    6. เลือกเมนู Truf
        ...    7. เลือกเมนู Validate Truf
    Open App
    Login
    Search
    
    

*** Keywords ***
Open App 
   Open Application	    http://localhost:4723/wd/hub	 platformName=Android    deviceName=emulator-5554   appPackage=th.co.truecorp.cpd.iservicemax	 appActivity=th.co.truecorp.cpd.iservicemax.MainActivity  autoGrantPermissions=true
   
Login
   Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_username"]  20 
   Input Text    //android.widget.EditText[@content-desc="input_username"]    39027299
   Input Text    //android.widget.EditText[@content-desc="input_password"]    Test=12345
   Click Element    //android.widget.Button[@content-desc="button_login"] 
   Wait Until Page Contains Element  //*[@text='Smart Visit']  20
   Capture Page Screenshot      none
Logout
   Click Element     //android.widget.TextView[@index='0']
   Click Element    //*[@text='Logout']
   Close Application

Select Truf
   Click Element    //android.widget.Button[@index='2']
   Wait Until Page Contains Element    //*[@text='Turf Code : 103801010001'] 
   Capture Page Screenshot      Truf.png

Select Validate Truf Member
   Click Element    //android.widget.TextView[@text='Turf Code : 103801010001']
   Wait Until Page Contains Element    //*[@text='employee']
   Capture Page Screenshot    Validate Truf.png

Search
   Click Element    //*[@text='Search']