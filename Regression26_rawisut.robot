*** Settings ***
Library    AppiumLibrary


*** Test Cases ***
Test Open app
    Open App
    Login
    Search
    Validate Search Result


*** Keywords ***
Open App
    Open Application	    http://localhost:4723/wd/hub	 platformName=Android    deviceName=emulator-5554   appPackage=th.co.truecorp.cpd.iservicemax	 appActivity=th.co.truecorp.cpd.iservicemax.MainActivity  autoGrantPermissions=true
   
Login
   Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_username"]  30 
   Input Text    //android.widget.EditText[@content-desc="input_username"]    39027299
   Input Text    //android.widget.EditText[@content-desc="input_password"]    Test=12345
   Click Element    //android.widget.Button[@content-desc="button_login"] 
   Wait Until Page Contains Element  //*[@text='Smart Visit']  20
   Capture Page Screenshot      none
Logout
   Click Element    //android.widget.TextView[@index='0']
   Click Element    //*[@text='Logout']
   Close Application

Search
   Click Element  //*[@text='Search']
   Wait Until Page Contains Element    //android.widget.EditText[@index='1'] 
   Input Text     //android.widget.EditText[@index='1']  กขค
   Click Element    //*[@text='ค้นหา']

Validate Search Result
   Clear Text    //android.widget.EditText[@index='1']
   Input Text    //android.widget.EditText[@index='1']  xx
   Click Element    //*[@text='ค้นหา']
   Wait Until Page Contains Element    //android.widget.ScrollView[@index='0'] 
   Capture Page Screenshot    xx.png
   Clear Text    //android.widget.EditText[@index='1']
   Input Text    //android.widget.EditText[@index='1']  กกกกก ขขขขข
   Click Element    //*[@text='ค้นหา'] 
   Wait Until Page Contains Element    //android.widget.ImageView[@index='1'] 
   Click Element    //*[@text='คุณลูกค้า : คุณ กกกกก ขขขขข']
   Wait Until Page Contains Element    //android.widget.TextView[@text='Account Type'] 
   Capture Page Screenshot   Customer.png 

   


