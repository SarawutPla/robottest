*** Settings ***
Library           AppiumLibrary

*** Test Cases ***
Test Open app
    [Documentation]    Open Application
    ...   Login
    ...       1. เข้า App iServiceMax
    ...       2. ใส่ Username และ Password
    ...       3. กดปุ่ม Login
    ...   Submit-Media-Visibility
    ...   STEP01: ตรวจสอบการกดไอคอนหมุดข้อมูลสื่อ
    ...       1. คลิกเมนู Task
    ...       2. เลือก Media
    ...       3. คลิกไอคอนเพิ่มหมุดข้อมูลสื่อ
    ...   STEP02: ตรวจสอบ Pop up เพิ่มจุดสำรวจ
    ...       1. แสดงหัวข้อ เพิ่มจุดสำรวจ
    ...       2. แสดงที่อยู่
    ...       3. แสดงไอคอนเพิ่มหมุดข้อมูลสื่อ
    ...  STEP03: ตรวจสอบการกดไอคอนเพิ่มหมุดข้อมูลสื่อ
    ...       1. แสดงหน้าใส่ข้อมูลในเรื่องสื่อมีเดียต่างๆ ทั้งของ ทรูและคู่แข่ง
   ...        2. แสดงรหัสรายการ
    ...       3. แสดงรหัสพนักงาน
    ...       4. แสดงชื่อ-นามสกุล
    ...       5. แสดงประเภทสินค้า
    ...       6. แสดงยี่ห้อ
    ...       7. แสดงประเภทสื่อมิเดีย
    ...       8. แสดงป้ายสื่อสาร (จุดขาย) เกี่ยวกับ 
    ...       9. แสดงจำนวนจุดตั้ง  Default เป็น 1
    ...      10. แสดงป้ายสื่อโฆษณา (ที่เป็นป้ายใหม่, อัพเดทข้อมูลใหม่ บนพื้นที่เดิม/ใหม่) 
    ...      11. แสดงคำอธิบายเพิ่มเติม
    ...      12. แสดงรูปภาพ
    ...      13. แสดงปุ่มยืนยัน
    ...      14. แสดงปุ่มยกเลิก"
  
    Open App
    Login
    #Search
    #Validate Search Result
    #Submit-Media-Visibility
    

*** Keywords ***
Open App
    Open Application    http://localhost:4723/wd/hub    platformName=Android    deviceName=emulator-5554    appPackage=th.co.truecorp.cpd.iservicemax    appActivity=th.co.truecorp.cpd.iservicemax.MainActivity    autoGrantPermissions=true

Login
    Wait Until Page Contains Element    //android.widget.EditText[@content-desc="input_username"]    15
    Input Text    //android.widget.EditText[@content-desc="input_username"]    39027299
    Input Text    //android.widget.EditText[@content-desc="input_password"]    Test=12345
    Click Element    //android.widget.Button[@content-desc="button_login"]
    Wait Until Page Contains Element    //*[@text='Smart Visit']    20
    Capture Page Screenshot    none

Logout
    Click Element    //android.widget.TextView[@index='0']
    Click Element    //*[@text='Logout']
    Close Application

Search
    Click Element    //*[@text='Search']
    Wait Until Page Contains Element    //android.widget.EditText[@index='1']
    Input Text    //android.widget.EditText[@index='1']    กขค
    Click Element    //*[@text='ค้นหา']

Validate Search Result
    Clear Text    //android.widget.EditText[@index='1']
    Input Text    //android.widget.EditText[@index='1']    xx
    Click Element    //*[@text='ค้นหา']
    Wait Until Page Contains Element    //android.widget.ScrollView[@index='0']
    Capture Page Screenshot    xx.png
    Clear Text    //android.widget.EditText[@index='1']
    Input Text    //android.widget.EditText[@index='1']    กกกกก ขขขขข
    Click Element    //*[@text='ค้นหา']
    Wait Until Page Contains Element    //android.widget.ImageView[@index='1']
    Click Element    //*[@text='คุณลูกค้า : คุณ กกกกก ขขขขข']
    Wait Until Page Contains Element    //android.widget.TextView[@text='Account Type']
    Capture Page Screenshot    Customer.png

Submit-Media-Visibility
    Wait Until Page Contains Element    //android.widget.TextView[@text='103801080000']    15
    Scroll    //android.widget.TextView[@text='103801080000']    //android.widget.TextView[@text='Happy Visit']
    #Wait Until Page Contains Element    //android.widget.TextView[@text='RP Route Visit']    15
    Click Element    //*[@text='Media Visibility']
    Wait Until Page Contains Element    //*[@class="android.widget.RelativeLayout"]
    Click Element    //hierarchy/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.LinearLayout/android.widget.FrameLayout/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.FrameLayout/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup/android.widget.ImageView
    Wait Until Page Contains Element    //android.widget.SeekBar[@content-desc="Bottom Sheet"]/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]
    Element Text Should Be    //android.widget.SeekBar[@content-desc="Bottom Sheet"]/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.widget.TextView[1]    เพิ่มจุดสำรวจ
    Element Text Should Be    //android.widget.SeekBar[@content-desc="Bottom Sheet"]/android.view.ViewGroup[3]/android.view.ViewGroup/android.view.ViewGroup/android.widget.ScrollView/android.view.ViewGroup/android.view.ViewGroup/android.view.ViewGroup[2]/android.widget.Button/android.widget.TextView    เพิ่มหมุดข้อมูลสื่อมีเดีย
    Capture Page Screenshot    ตรวจสอบ Pop up เพิ่มจุดสำรวจ.png
    Click Element    //android.widget.Button[@index="0"]
    #Element Text Should Be    //