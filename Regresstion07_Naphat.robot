*** Settings ***
Library  AppiumLibrary


*** Test Cases ***
Test Open Application
    [Documentation]  Select Smart Visit
    ...              1.Login
    ...              2.เลือก Task
    ...              3.เลือก Smart Visit
    ...              4.เลือกบ้านเลขที่ หรือ Search
    ...              5.เลือกบ้านเลขที่ ที่ต้องการ"

    Open-Application
    Login
    Select-Menu
    Select-Visit
    Select-List
    Selected

*** Keywords ***
Open-Application
    Open Application  http://localhost:4723/wd/hub    platformName=Android    platformVersion=12  deviceName=emulator-5554    appPackage=th.co.truecorp.cpd.iservicemax  appActivity=th.co.truecorp.cpd.iservicemax.MainActivity  autoGrantPermissions=true

Login    
    Wait Until Page Contains Element  //android.widget.TextView[@text='Login']  30
    Input Text  //*[@text='Username']  39027299
    Input Text  //*[@text='Password']  Test=12345
    Click Element  //*[@text='Login']
    Wait Until Page Contains Element  //android.view.ViewGroup
    Capture Page Screenshot     filename=None

Select-Menu
    Wait Until Page Contains Element  //android.widget.TextView[@text='Task']  40
    Click Element  //android.widget.TextView[@text='Task']

Select-Visit
    Wait Until Page Contains Element  //android.widget.TextView[@text='103801010000']  40
    Click Element  //android.widget.TextView[@text='103801010000']
    
Select-List
    Wait Until Page Contains Element  //android.widget.ImageView[@index='0']  40
    Click Element  //android.widget.TextView[@text='List']

Selected
    Wait Until Page Contains Element  //android.widget.TextView[@text='51/179, ซ.รามอินทรา 34, ถ.อยู่เย็น, ลาดพร้าว, ลาดพร้าว, กรุงเทพมหานคร, 10230']  40
    Click Element  //android.widget.TextView[@text='']
    Capture Page Screenshot     filename=None