*** Settings ***
Library  AppiumLibrary


*** Test Cases ***
Test Open Application
    [Documentation]  Select Truf Member
    ...              1.Login
    ...              2.เลือก เมนู Truf
    ...              3.เลือก Truf Member

    Open-Application
    Login
    Select-Menu
    Select-Truf-Member

*** Keywords ***
Open-Application
    Open Application  http://localhost:4723/wd/hub    platformName=Android    platformVersion=12  deviceName=emulator-5554    appPackage=th.co.truecorp.cpd.iservicemax  appActivity=th.co.truecorp.cpd.iservicemax.MainActivity  autoGrantPermissions=true

Login    
    Wait Until Page Contains Element  //android.widget.TextView[@text='Login']  30
    Input Text  //*[@text='Username']  39027299
    Input Text  //*[@text='Password']  Test=12345
    Click Element  //*[@text='Login']
    Wait Until Page Contains Element  //android.view.ViewGroup
    Capture Page Screenshot     filename=None

Select-Menu
    Wait Until Page Contains Element  //android.widget.TextView[@text='Turf']  40
    Click Element  //android.widget.Button[@index='2']

Select-Truf-Member
    Wait Until Page Contains Element  //android.widget.TextView[@text='Turf Code : 103801010001']  40
    Click Element  //android.widget.TextView[@text='Turf Code : 103801010001']
    Capture Page Screenshot     filename=None