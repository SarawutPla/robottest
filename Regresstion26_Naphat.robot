*** Settings ***
Library  AppiumLibrary


*** Test Cases ***
Test Open Application
    [Documentation]  Open Application
...                  Login
...                      1. เข้า App iServiceMax
...                      2. ใส่ Username และ Password
...                      3. กดปุ่ม Login
...                 Search
...                     1.เลือก เมนู Search
...                     2.ใส่ข้อมูลที่ต้องการค้นหา
...                     3.กดปุ่ม ค้นหา
...                 Validate-Search-Result
...                      1.ช่อง Search แสดงผลการค้นหา xx
...                      2.List
...                       - แสดง List เท่ากับ ผลการค้นหา xx
...                       - แสดง ชื่อ (คุณลูกค้า : กกกกก  ขขขขข)
...                       - แสดง เลนบัตรประจำตัว (เลขบัตรประจำตัว : X-XXXX-XXXX1-23-4)
...                       - แสดง Customer Type (Individual,Corporate,Business)"


    Open-Application
    Login
    Select-Menu
    Search-Input

*** Keywords ***
Open-Application
    Open Application  http://localhost:4723/wd/hub    platformName=Android    platformVersion=12  deviceName=emulator-5554    appPackage=th.co.truecorp.cpd.iservicemax  appActivity=th.co.truecorp.cpd.iservicemax.MainActivity  autoGrantPermissions=true

Login    
    Wait Until Page Contains Element  //android.widget.TextView[@text='Login']  30
    Input Text  //*[@text='Username']  39027299
    Input Text  //*[@text='Password']  Test=12345
    Click Element  //*[@text='Login']
    Wait Until Page Contains Element  //android.view.ViewGroup
    Capture Page Screenshot     filename=None

Select-Menu
    Wait Until Page Contains Element  //android.widget.TextView[@text='Search']  40
    Click Element   //android.widget.Button[@index='3']
    
Search-Input
    Wait Until Page Contains Element  //android.widget.TextView[@text='กรุณาระบุชื่อ, หมายเลขบัตรประชาชน,']  40
    Click Element  //android.widget.EditText[@text='กรุณาระบุข้อมูลที่ต้องการค้นหา']
    Input Text  //android.widget.EditText[@text='กรุณาระบุข้อมูลที่ต้องการค้นหา']  191
    Click Element  //android.widget.TextView[@text='ค้นหา'] 
    Capture Page Screenshot     filename=None