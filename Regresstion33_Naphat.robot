*** Settings ***
Library  AppiumLibrary


*** Test Cases ***
Test Open Application
    [Documentation]  Open Application
    ...              Login
    ...                  1. เปิด App iServiceMax
    ...                  2. ใส่ Username และ Password
    ...                  3. กดปุ่ม login
    ...              Select Task Happy Visit
    ...                  1. เลือก Task
    ...                  2. เลือก Happy Visit ที่ต้องการ

    Open-Application
    Login
    Select-Task

*** Keywords ***
# OPENAPP -------------- #
Open-Application
    Open Application  http://localhost:4723/wd/hub    platformName=Android    platformVersion=12  deviceName=emulator-5554    appPackage=th.co.truecorp.cpd.iservicemax  appActivity=th.co.truecorp.cpd.iservicemax.MainActivity  autoGrantPermissions=true

Login    
    Wait Until Page Contains Element  //android.widget.TextView[@text='Login']  15
    Input Text  //*[@text='Username']  39027299
    Input Text  //*[@text='Password']  Test=12345
    Click Element  //*[@text='Login']
    Wait Until Page Contains Element  //android.view.ViewGroup
    Capture Page Screenshot     filename=None

Select-Task
    Wait Until Page Contains Element  //android.widget.TextView[@text='Task']  30
    # Click Element   //android.widget.TextView[@text='Task']
    Click Element  //android.widget.TextView[@text='True Merchant']
    Capture Page Screenshot     filename=None
 
# Logout
#     Wait Until Page Contains Element  //*[@text='Smart Visit']  15
#     Click Element  //android.widget.TextView[@index='0']
#     Wait Until Page Contains Element  //android.widget.ImageView
#     Click Element  //*[@text='Logout']
#     Close Application 

